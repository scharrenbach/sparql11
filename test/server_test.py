#!/usr/bin/env python3
# coding:utf-8

"""sparql11 unittests"""

__author__ = "Thomas Scharrenbach"
__copyright__ = "Copyright (C) 2014 Thomas Scharrenbach"
__license__ = "Apache License v2"
__version__ = "0.0.1"

import unittest

import logging as _log

from urllib.error import HTTPError

import sparql11.server

from urllib.request import urlopen

from urllib.parse import urlencode

import traceback

import itertools

import sys

import logging as log

root = log.getLogger()
root.setLevel(log.DEBUG)

ch = log.StreamHandler(sys.stdout)
ch.setLevel(log.DEBUG)
formatter = log.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

class TestServer(unittest.TestCase):
	"""Unit tests for SPARQLEndpoint.

	"""

	def setUp(self):
		self.host = 'localhost'
		self.port = 3030

		args = sparql11.server.parse_args(
		['{"default":"file:./test.ttl"}','--host=localhost','--port=3030'])
		self.server = sparql11.endpoint(args.dataset, args.hostname, args.port)
		self.server.start()

	def tearDown(self):
		try:
			_log.info('Shutting down http server')
			self.server.shutdown()
		except:
			_log.error('Error shutting down http server!')
			traceback.print_exc(file=sys.stdout)

	def _execute_query(self, query, result_format):
		params = urlencode({'query': query, 'format': result_format, 'timeout':'30000'})
		scheme = 'http'
		host = self.host
		port = self.port
		path = '/'

		GET = True
		try:
			if GET:
				request_uri = '{scheme}://{host}:{port}{path}?{params}'.format(
					scheme = scheme, host=host, port=port, path=path, params=params
				)
				response = urlopen(request_uri)
			else:
				request_uri = '{scheme}://{host}:{port}{path}'.format(
					scheme = scheme, host=host, port=port, path=path
				)
				response = urlopen(request_uri, data=params.encode('utf-8'))

			text = response.read()
			response.close()
			print('Response: {}'.format(text.decode('UTF-8')))
		except HTTPError as e:
			print(e)


	def test_server(self):
		_log.info('Started test for server...')

		queries_list = list()
		queries_list.append('SELECT * WHERE { <http://dbpedia.org/resource/Albert_Einstein> ?p ?o }')
		queries_list.append('CONSTRUCT { <http://dbpedia.org/resource/Albert_Einstein> ?p ?o .} '
							'WHERE { <http://dbpedia.org/resource/Albert_Einstein> ?p ?o }')

		result_formats_list = list()
		result_formats_list .append('application/sparql-results+xml')
		result_formats_list .append('text/csv')
		result_formats_list .append('application/rdf+xml')

		try:
			for query, result_format in itertools.product(queries_list, result_formats_list):
				self._execute_query(query, result_format)

		except:
			_log.error('Error testing server!')
			traceback.print_exc(file=sys.stdout)

		pass
if __name__=='__main__':
	unittest.main()
	import sys
	sys.exit(0)