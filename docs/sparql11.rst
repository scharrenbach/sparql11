.. sparql11
   (C) 2014 Thomas Scharrenbach
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
..
        http://www.apache.org/licenses/LICENSE-2.0
..
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

sparql11 package
================

Module contents
---------------

.. autofunction:: sparql11.endpoint
.. autofunction:: sparql11.parse_args
.. autoclass:: sparql11.QueryHandler
    :members:
    :undoc-members:
    :show-inheritance:
.. autoclass:: sparql11.SPARQLEndpoint
    :members:
    :undoc-members:
    :show-inheritance:
