.. sparql11
   (C) 2014 Thomas Scharrenbach
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
..
        http://www.apache.org/licenses/LICENSE-2.0
..
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Quickstart
==========

Installation
------------

::

  pip install sparql11


Usage
-----

It's really simple to expose your graph via a SPARQL endpoint:
::

  import sparql11
  from rdflib import Graph

  g = Graph()

  # start a new SPARQL endpoint for g at localhost:3030
  endpoint = sparql11.endpoint(g)



Documentation on how to perform queries follows soon.